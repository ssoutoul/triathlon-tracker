import React from 'react';

import {View , StyleSheet, Text} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Foundation from 'react-native-vector-icons/Foundation';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Octicons from 'react-native-vector-icons/Octicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Icon } from 'react-native-elements';
import AddWorkouts from './AddWorkouts/AddWorkouts'
export default class Footer extends React.Component {


    render(){
      return (
        <View style ={styles.container}>
            <View style ={styles.aline}>
            
            <Foundation onPress = 
            {() =>  this.props.changePage('Results')} name = "graph-trend" size ={45} color='white'/> 
            <Entypo  onPress = 
            {() =>  this.props.changePage('Goal')} name = "trophy" size ={45} color ="white"/> 
            <Ionicons onPress=
            {() => this.props._toggleModal()} style ={{top:-7}} name = "md-add-circle" size ={60} color ="white" /> 
            <Octicons   onPress = 
            {() =>  this.props.changePage('Workouts')} name = "checklist" size ={45} color ="white" /> 
            <MaterialCommunityIcons  onPress = 
            {() =>  this.props.changePage('Alerts')} name = "bell-plus" size ={45} color ="white" /> 
            
            </View>

            
        </View>
        )
    }
  }
  const styles = StyleSheet.create({
    container: {
      flex:.1,
      borderTopWidth: 2,
      borderTopColor: 'white'
      
    },
    aline: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        top:9

      },


  });

  