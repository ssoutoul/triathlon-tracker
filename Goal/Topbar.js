import React from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { StyleSheet, Text, View, Alert, TextInput, Button} from 'react-native';
import { Header } from 'react-native-elements';
import Modal from "react-native-modal";
import Choosesport from './Choosesport'
import GoalsAPI from '../api/GoalsAPI'


const icons   = {
  'Swim': 'swim',
  'Bike': 'directions-bike',
  'Run':'directions-run'
 }
 
 const colors   = {
  'Swim': '#2DBFA4',
  'Bike': '#E500A6',
  'Run':'#820384'
 }

export default class Navbar extends React.Component {

     state = {
        isModalVisible: false,
        text: 'Placeholder'
       };
       
       collectData = (action, data) => {
         
        this.setState({[action]:data})
        
      }

      _toggleModal = () =>{
          this.setState({ isModalVisible: !this.state.isModalVisible });
      }

      
      add = () =>{
        var self = this
        let {sport, distance} = this.state
        
         GoalsAPI.find({sport:sport},(err, data)=>{
           
           if(data.length < 1){
            
              var icon = icons [sport]
              var color = colors [sport]
              GoalsAPI.insert({
                sport, 
                distance,
                icon,
                color,
                swimtype : 'material-community'
              
              },(err,done)=>{
                this.props.findAll()  
                self._toggleModal()
              })
        
           }else{
            
            Alert.alert(
              'Error',
              "You can't add anymore of that Sport",
              [
                {text: 'OK', onPress: () => {
                  self._toggleModal()
                }},
                ],
              { cancelable: false }
            )
           }
           

         })

       

      }

    render() {
      return (
    
            <View >
<Header
outerContainerStyles={{borderBottomWidth:0}}
centerComponent={{ text: 'Goals', style: { color: '#fff',fontSize:25 } }}
rightComponent={{ icon: 'add', color: '#fff', onPress:this._toggleModal}} 
backgroundColor = '#1C1C21'

/>
                
                <Modal
            animationInTiming = {50}
            animationOutTiming = {50}
            animationType={"slide"}
            backdropColor = '#1C1C21' 
            backdropOpacity = {1}
            transparent={false} 
            isVisible={this.state.isModalVisible}>
            
              <View style={{ flex: 1}}>
                
                <Header
                    leftComponent= {{ icon: 'cancel', size: 30,color: '#fff', onPress: this._toggleModal}}
                    centerComponent={{ text: 'Add Goal', style: { color: '#fff',fontSize: 25} }}
                    backgroundColor = '#1C1C21'
                  
                />
             

                <View style = {{flex: 0.6,justifyContent: 'space-around'}}>
                
                <View style = {{flexDirection: 'row',justifyContent: 'space-between'}}>
                    <Text style = {{color:'white',fontSize:20, fontWeight: 'bold'}}>SPORT</Text>
                    <Choosesport 
                    collectData = {this.collectData} />
                  </View>
                
                 
                  <View style = {{flexDirection: 'row',justifyContent: 'space-between'}}>
                  <Text style = {{color:'white',fontSize:20, fontWeight: 'bold'}}>Distance</Text>
                  <TextInput 
                        style={{height:40, width:150, backgroundColor:'#1C1C21',color:'white'}}
                        placeholder="ex.20 km"
                        placeholderTextColor='#D8D8D8'
                        borderBottomWidth={1}
                        borderBottomColor='white'
                        onChangeText={(text) => this.collectData('distance',text)}
                        keyboardType='numeric'
                        returnKeyType = 'done'
                    
                    />
                  </View>
                    

                  </View>
                  
                </View>
                <View  style = {{flex: 0.1,justifyContent: 'center',alignItems: 'center', borderWidth:1, borderColor:'white'}}>
                  <Button 
                   buttonStyle = {{width: 300}}
                   borderRadius = {5}
                   color = '#2DBFA4'
                   backgroundColor = 'white'
                   title='Save Goal' 
                   onPress={()=>{
                     this.add()
                    }}
                   textStyle = {{fontSize: 30,fontWeight: 'bold'}}/>
                </View>
                </Modal>
        </View>
  
        
      );
    }
  }


