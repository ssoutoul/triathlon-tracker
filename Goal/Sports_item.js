import React from 'react';
import { Icon } from 'react-native-elements'
import { StyleSheet, Text, View,TextInput, Button} from 'react-native';
import ProgressBar from 'react-native-progress/Bar';
import { Header } from 'react-native-elements';
import Modal from "react-native-modal";
import Choosesport from './Choosesport'
import GoalsAPI from '../api/GoalsAPI'
import WorkoutsCollections from '../api/WorkoutsCollection'

export default class Sports_items extends React.Component {
  state = {
    isModalVisible: false,
    
   }
   
  _toggleModal = () =>{
      this.setState({ isModalVisible: !this.state.isModalVisible });
  }
  collectData = (action, data) => {
    
   this.setState({[action]:data})
   
 }
 deleteWorkout = (sport) => {
  
  WorkoutsCollections.remove({sport:this.props.sports.sport},{multi: true}, (e,d) => {
    this.props.findWorkouts()
  })
}

handleRemove = () => {    
  GoalsAPI.remove({_id:this.props.sports._id},(err,done)=>{
    this.props.findAll()  
  })
}
// update
update = () =>{
  
GoalsAPI.update({_id:this.props.sports._id},{$set:{
  distance:this.state.distance
}},(err, done)=>{
  this.props.findAll()  
})
}

  
    render() {
        let { last, index } = this.props
        let marginLeft;
        
        let color;
        if(last != index){
          color = 'white'
        } else {
          color = 'transparent'
        }
        if(this.props.sports.icon == 'swim'){
          marginLeft = 0
        }else{
          marginLeft = 0;
        }
        return (
          <View style={{   
           borderBottomColor: color,
          borderBottomWidth:2,
          height:180

          }}>
            <View >
            <View style = {styles.text}>
                <Text style = {{color: 'white',fontSize:17}}>{this.props.sports.distance + ' Km'}</Text>
                </View>
             <View style = {styles.items}>
                <View style = {{top:0}}>
                {this.props.sports.icon == 'swim'  ?<Icon 
                    name = {this.props.sports.icon}
                    color = {this.props.sports.color}
                    type= {this.props.sports.swimtype}
                    size = {90}
                    // onPress = {this.props.totaldistance('Swim')}    
                    /> : 
                    <Icon 
                    name = {this.props.sports.icon}
                    color = {this.props.sports.color}
                    size = {90}
              
                    
                    />}
                </View>
                <View>
                <ProgressBar 
                top={45} 
                progress={this.props.sports.progress} 
                color= {this.props.sports.color}
                width={200} />
                </View>
                
               </View>
               <View style ={styles.bottom}>
                    <Text style = {{fontSize:17,color: 'white',top:30, width:110, textAlign:'center'}}>{this.props.sports.sport}</Text>
                <View>
                    <Text style = {{
                      color: 'white', 
                      right:marginLeft, 
                      width:200,
                      fontSize:17,
                      top:-5,
                      textAlign:'center',
                      }}>{this.props.sports.duration}</Text>
                </View>
                <Icon 

                    name = 'create'
                    type= 'MaterialIcons'
                    size = {30}
                    color ='white'
                    top = {20}
                    onPress={this._toggleModal}

                    
                    />}
                    <Modal
            animationInTiming = {50}
            animationOutTiming = {50}
            animationType={"slide"}
            backdropColor = '#1C1C21' 
            backdropOpacity = {1}
            transparent={false} 
            isVisible={this.state.isModalVisible}>
            
              <View style={{ flex: 1}}>
    
                <Header
                    leftComponent= {{ 
                      icon: 'cancel', size: 30,color: '#fff', onPress: this._toggleModal
                    }}
                    centerComponent={{ 
                      text: 'Edit', style: { color: '#fff',fontSize: 25} }}
                    rightComponent= {{
                       icon: 'delete', size: 30,color: '#fff', 
                       onPress: ()=>{
                        this.handleRemove()
                        this._toggleModal()
                        this.deleteWorkout()
                        
                       }
                      }}
                    backgroundColor = '#1C1C21'
                  
                />
    
                <View style = {{flex: 0.6,justifyContent: 'space-around'}}>
                
                  <View style = {{flexDirection: 'row',justifyContent: 'space-between'}}>
                  <Text style = {{color:'white',fontSize:20, fontWeight: 'bold'}}>Distance</Text>
                  <TextInput 
                        style={{height:40, width:200, backgroundColor:'#1C1C21',color:'white'}}
                        placeholder="In km"
                        placeholderTextColor='#D8D8D8'
                        borderBottomWidth={1}
                        borderBottomColor='white'
                        onChangeText={(text) => this.collectData('distance',text)}
                        keyboardType='numeric'
                        returnKeyType = 'done'
                        
                    />
                  </View>

                  </View>
                  
                </View>
                <View  style = {{flex: 0.1,justifyContent: 'center',alignItems: 'center', borderWidth:1, borderColor:'white'}}>
                  <Button 
                   buttonStyle = {{width: 300}}
                   borderRadius = {5}
                   color = '#2DBFA4'
                   backgroundColor = 'white'
                   title='Save Changes' 
                   onPress={()=>{
                    this.update()
                    this._toggleModal()
                   }}
                   textStyle = {{fontSize: 30,fontWeight: 'bold'}}/>
                </View>
                </Modal>
                </View>
             </View>
          </View> 
        ) 
    }
}

  const styles = StyleSheet.create({
    text: {
        justifyContent: 'center',
        alignItems:'center',
        top:15,
        left:40
        

        
      },
      items: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        left:-15,
        top:25
      },
      bottom:{
        flexDirection: 'row',
        // justifyContent: 'center',
        // alignItems:'center',
        justifyContent: 'space-between',

      }
});