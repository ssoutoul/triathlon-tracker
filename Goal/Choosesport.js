import React from 'react';
import { Text, TouchableOpacity,TouchableHighlight, View, Picker,StyleSheet} from "react-native";
import Modal from "react-native-modal";
import { Header, Button } from 'react-native-elements';

export default class AddSport extends React.Component {
    state = {
        isModalVisible: false
      };

   
    
      _toggleModal = () =>
        this.setState({ isModalVisible: !this.state.isModalVisible });
    
      render() {
        return (
          <View>
 
            <Button  buttonStyle = {{width: 150}} outline = {true} onPress={this._toggleModal} title={this.state.language || 'Choose Sport'} color = '#2DBFA4' backgroundColor = 'transparent'/>
            <Modal  isVisible={this.state.isModalVisible}
            
             >
              <View>
    
                <View>
                 <Picker
                    style =  {{ width: '80%', alignSelf:'center',backgroundColor: 'white',borderColor: 'gray',borderWidth: 1}}
                    itemStyle = {{color: 'black'}}
                    selectedValue={this.state.language}
                    onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}>
                    <Picker.Item label="Swim" value="Swim" />
                    <Picker.Item label="Bike" value="Bike" />
                    <Picker.Item label="Run" value="Run" />
                      
                </Picker>
               
                </View>
                <TouchableOpacity>
                  <View style = {{flexDirection: 'row'}}>
                    <Button  onPress={this._toggleModal} backgroundColor = 'white' color = 'black' buttonStyle = {{width: 135,left:19,borderColor: 'gray',borderWidth: 1}} title='Cancel'/>
                    <Button   onPress={()=>{
                     this.props.collectData('sport',this.state.language)
                     this._toggleModal()
                   }} backgroundColor = 'white' color = '#2DBFA4' buttonStyle = {{width: 135,right:11,borderColor: 'gray',borderWidth: 1}} title='Save'/>
                  </View>
                </TouchableOpacity>
              </View>
            </Modal>
          </View>
        );
      }
}

