import React from 'react';
import Sports_item from './Sports_item'
import { StyleSheet, Text, View } from 'react-native';

export default class Sports_list extends React.Component {

    _renderSports = () => {
        debugger
        return this.props.sports.map ((el,index, arr)=>{
            
            el.index = index
            return <Sports_item 
                key = {index}
                sports = {el}
                last = {arr.length -1}
                index = {index}
                findAll={this.props.findAll}  
                findWorkouts = {this.props.findWorkouts}
                // totaldistance = {this.props.totaldistance}
                // totalpercentage = {this.props.totalpercentage}    

               
            />
        }) 
    }
    render() {
        
        return (
          <View>
             {this._renderSports()}
          </View>
    
          
        )
      
    }


}