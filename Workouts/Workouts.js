import React from 'react';
import Workouts_list from './Workouts_list'
import { StyleSheet, Text, View} from 'react-native';
import { Header } from 'react-native-elements';
import WorkoutsCollections from '../api/WorkoutsCollection'

export default class Workouts extends React.Component {
 

    render() {
        
   
    
      return (
        <View style = {styles.workouts}>
             <Header
                    centerComponent={{ text: 'Workouts', style: { color: '#fff',fontSize: 25} }}
                    backgroundColor = '#1C1C21'
                  
                />
        
                <Workouts_list 
                    deleteWorkouts = {this.props.deleteWorkouts}
                    workouts = {this.props.workouts}
                    _toggleModal = {this.props._toggleModal}
                    findID = {this.props.findID}
                />


            
        </View>
        
  
        
      );
    }
  }

  const styles = StyleSheet.create({
    workouts: {
        flex:1
    }
  })

  

  
  