import React from 'react';
import Workouts_items from './Workouts_items'
import { StyleSheet, Text, View,ScrollView} from 'react-native';
import WorkoutsCollections from '../api/WorkoutsCollection';

export default class Workouts_list extends React.Component {

    _renderWorkouts = () => {
        
        return this.props.workouts.map ((el,i)=>{
            
            el.index = i
            return <Workouts_items 
                i = {i}
                workouts = {el}
                _id ={el._id}
                deleteWorkouts = {this.props.deleteWorkouts}
                _toggleModal = {this.props._toggleModal}
                findID = {this.props.findID}
               
            />
        }) 
    }
    render() {
        
        return (
          <ScrollView>
             {this._renderWorkouts()}
          </ScrollView>
    
          
        )
      
    }


}

const styles = StyleSheet.create({
    contentContainer: {
      paddingVertical: 20,
      flex: 0.9
    }
  });