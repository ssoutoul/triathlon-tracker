import React from 'react';
import { Icon } from 'react-native-elements'
import { StyleSheet, Text, View} from 'react-native';
import WorkoutsCollections from '../api/WorkoutsCollection';

export default class Workouts_items extends React.Component {

    render() {
        
        return (
          <View style = {{height: 107,borderBottomColor: 'white', borderBottomWidth: 2}}>
             <View style = {styles.items}>
                <View style = {{top: 20}}>
                {this.props.workouts.icon == 'swim'  ?<Icon 
                    name = {this.props.workouts.icon}
                    color = {this.props.workouts.color}
                    type= {this.props.workouts.swimtype}
                    size = {60}
                    
                    /> : 
                    <Icon 
                    name = {this.props.workouts.icon}
                    color = {this.props.workouts.color}
                    size = {60}

                    
                    />}
                </View>
                <View>
                    <Text style = {{color: 'white', fontSize: 16,top: 20}}>Distance: {this.props.workouts.distance} km</Text>
                    <Text style = {{color: 'white', fontSize: 16,top: 30}}>Duration: {this.props.workouts.time}</Text>
                </View>
                <View style = {{top: 20}}>
                    <Icon 
                    name = 'trash-o'
                    type = 'font-awesome'
                    color = {this.props.workouts.color}
                    size = {40}
                    onPress = {() => this.props.deleteWorkouts(this.props._id)}
                    underlayColor = '#1C1C21'
 
                    />
                </View>
                <View style = {{top: 22}}>
                    <Icon 
                    name = 'edit'
                    type = 'font-awesome'
                    color = {this.props.workouts.color}
                    size = {40}
                    onPress = {() => {
                    this.props._toggleModal()
                    this.props.findID(this.props._id)}} 
                    />

                </View>
             </View>
          </View>
    
          
        )
      
    }

}


const styles = StyleSheet.create({
    items: {
       flexDirection: "row",
       justifyContent: 'space-around'
       
    }
  })
