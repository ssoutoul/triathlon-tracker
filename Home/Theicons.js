import React from 'react';
import { Icon } from 'react-native-elements'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { StyleSheet, Text, View} from 'react-native';

export default class Theicons extends React.Component {
    render() {
      return (
            <View style = {styles.navbar}>
                <View style = {styles.icons}>
              
                <Icon 
                    reverse
                    name='swim' 
                    type= 'material-community'
                    color ='#2DBFA4'
                    size = {25}
                   
                />
              
                <Icon 
                    reverse
                    name='directions-bike' 
                    color = '#E500A6'
                    size = {25}
                
                />
                <Icon 
                    reverse
                    name='directions-run' 
                    color='#820384'
                    size = {25}
                />
            </View>
        </View>
  
        
      );
    }
  }

  const styles = StyleSheet.create({
    navbar: {
      flex: 0.3
    },
    icons: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-around',
        top: 8
    }
  });