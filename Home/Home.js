import React from 'react';
import {View , Text, StyleSheet} from 'react-native';
import Emptytop from './Emptytop'
import Emptybottom from './Emptybottom'
import Theicons from './Theicons'

import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
export default class Home extends React.Component {

    render(){
        return (

<View style ={styles.thebody}>
<View style ={styles.thetop}>
    <Emptytop/>
    
</View>
<View style = {styles.toptext}>
    <View style = {styles.moreflex} >
        <Theicons/>
    </View>
    <View>
   <View>
    <Text style = {{color:'white', textAlign:'center', fontSize:30,top:30}}>
        Welcome
    </Text>
   </View>
   <View style = {{top:60}}>
    <Text style = {{color:'white', fontSize:30,textAlign:'center',fontStyle:'italic'}}>
    The fitness tracker for Triathletes

    </Text>
   </View>
   <View style ={{top:110,backgroundColor:'black',width:'70%',left:45}}>
   <View style = {{alignContent:'center'}}>
   <Button
  title='Setup Goals'
  fontSize={25}
  backgroundColor='black'
/>

</View>
    </View>
   </View>

<View style = {styles.moreflex2} >
        <Theicons/>
    </View>
</View>

<View style ={styles.bottomtext}>
 <Emptybottom/>   
</View>
</View>


)}

}
const styles = StyleSheet.create({  

thebody: {
  flex: 1,
 
},
thetop: {
    flex: 0.1,
   
  },
  moreflex:{
    flex:0.25
    
      },
  toptext:{
    borderTopWidth: 0.5,
    borderTopColor: 'white',
    width:'80%',
    marginLeft:'10%',
    flex: 0.784,
  },
  bottomtext:{
    flex: 0.1
  },
  moreflex2:{
    top:145
    
      },

})