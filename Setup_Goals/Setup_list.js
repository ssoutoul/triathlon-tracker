import React from 'react';
import Setup_item from './Setup_item'
import { StyleSheet, Text, View } from 'react-native';

export default class Setup_list extends React.Component {

    _renderSetup = () => {
        
        return this.props.setup.map ((el,index,)=>{
            el.index = index
            return <Setup_item 
                key = {index}
                setup = {el}
                

               
            />
        }) 
    }
    render() {
        
        return (
          <View>
             {this._renderSetup()}
          </View>
    
          
        )
      
    }


}