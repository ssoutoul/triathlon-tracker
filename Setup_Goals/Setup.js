import React from 'react';
import {View , Text, StyleSheet,TextInput} from 'react-native';
import Setup_list from './Setup_list';
import { Button } from 'react-native-elements';
import Top from './Top';

import { Icon } from 'react-native-elements'

export default class Setup extends React.Component {
    state = {
        setup: 
        [
          {category: 'Swimming', name: 'swim',type:'material-community',
        color: '#2DBFA4',
           
        },
    
        {category: 'Biking', name: 'directions-bike',
        color: '#E500A6',
        },
        {category: 'Running', name: 'directions-run',
        color: '#820384'}
    ]
        
      }
      state = {
        isModalVisible: false
      };

   
    
 
    
    render(){
        return (
<View style ={styles.thebody}>
<View style ={styles.thetop}>
    <Top/>
    
</View>    
    <View style ={styles.main}>
    <Setup_list                  
    setup = {this.state.setup}
   />
    </View>
    <View style ={{backgroundColor:'black',width:'70%',left:50,top:-50}}>
   <View style = {{alignContent:'center'}}>
   <Button
  title='Setup Goals'
  fontSize={25}
  backgroundColor='black'
/>
</View>
    </View>
</View>

)
}

}
const styles = StyleSheet.create({  
thebody: {
        flex: 1,
           
    },
thetop: {
    flex: 0.1,
       
},
main:{
    flex:0.9,
    borderTopWidth: 0.5,
    borderTopColor: 'white',
    width:'80%',
    marginLeft:'10%',
},
minititles:{
    width: '100%',
    flexDirection:'row',
    top:20
    

}  

})