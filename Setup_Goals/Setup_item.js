import React from 'react';
import {View , Text, StyleSheet,TextInput} from 'react-native';
import Top from './Top';

import { Icon } from 'react-native-elements'

export default class Setup extends React.Component {

    render(){
        return (
<View style ={{height:150}}>
    <View style = {styles.minititles}>
        <Text style ={{color:'white', width:'50%'}}>{this.props.setup.category}</Text>
        <Text style ={{color:'white', width:'50%'}}>Distance</Text>
    </View>
    <View style = {styles.minititles}>

    <Icon 
        reverse
        name={this.props.setup.name} 
        type= {this.props.setup.type} 
        color ={this.props.setup.color} 
        size = {25}
                   
    />

    <TextInput 
        style={{height:40, width:150, backgroundColor:'#1C1C21',color:'white'}}
        placeholder="Distance"
        placeholderTextColor='#D8D8D8'
        left={50}
        top={15}
        borderBottomWidth={0.5}
        borderBottomColor='white'
    
    />
    </View>
  </View>

)}

}
const styles = StyleSheet.create({  
thebody: {
        // flex: 1,
           
    },
thetop: {
    // flex: 0.1,
       
},
main:{
    // flex:0.9,
    // borderTopWidth: 0.5,
    // borderTopColor: 'white',
    // width:'80%',
    // marginLeft:'10%',
},
minititles:{
    width: '100%',
    flexDirection:'row',
    top:20,
    
   
    

}  

})