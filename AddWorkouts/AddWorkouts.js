import React from 'react';
import { Text, TouchableOpacity, View, Picker} from "react-native";
import Modal from "react-native-modal";
import { Header,Button } from 'react-native-elements';
import AddSport from './AddSport'
import AddTime from './AddTime'
import Adddistance from './Adddistance'
import WorkoutsCollections from '../api/WorkoutsCollection'

export default class AddWorkouts extends React.Component {
   

      render() {
        return (
          <View>
          
            <Modal
            animationInTiming = {50}
            animationOutTiming = {50}
            animationType={"slide"}
            backdropColor = '#1C1C21' 
            backdropOpacity = {1}
            transparent={false} 
            isVisible={this.props.isModalVisible}>
            
              <View style={{ flex: 1}}>
                
                <Header
                    leftComponent= {{ icon: 'cancel', size: 30,color: '#fff', onPress: () => this.props._toggleModal()}}
                    centerComponent={{ text: 'Add Workout', style: { color: '#fff',fontSize: 25} }}
                    backgroundColor = '#1C1C21'
                  
                />
             
                <View style = {{flex: 0.8,justifyContent: 'space-around'}}>
                  <View style = {{flexDirection: 'row',justifyContent: 'space-between'}}>
                    <Text style = {{color:'white',fontSize:20, fontWeight: 'bold'}}>SPORT</Text>
                    <AddSport 
                    collectData = {this.props.collectData}/>
                    
                    }
                  </View>
                  <View style = {{flexDirection: 'row',justifyContent: 'space-between'}}>
                    <Text style = {{color: 'white',fontSize:20,fontWeight: 'bold'}}>DISTANCE</Text>
                    <Adddistance collectData = {this.props.collectData}/>
                   
                  </View>
                  <View style = {{flexDirection: 'row',justifyContent: 'space-between'}}>
                    <Text style = {{color:'white',fontSize:20,fontWeight: 'bold'}}>TIME</Text>
                    <AddTime collectData = {this.props.collectData}/>
                    
                  </View>
                  
                </View>
                <View  style = {{flex: 0.2,justifyContent: 'center',alignItems: 'center'}}>
                  <Button
                   onPress={()=>{
                    this.props.insertWorkouts()
                    this.props._toggleModal()
                  }} 
                   
                   buttonStyle = {{width: 300}}
                   borderRadius = {5}
                   color = '#2DBFA4'
                   backgroundColor = 'white'
                   title='Save Workout' 
                   textStyle = {{fontSize: 20,fontWeight: 'bold'}}/>
                </View>
              </View>
            </Modal>
          </View>
        );
      }

}