import React from 'react';
import { Text, TouchableOpacity, View, Picker} from "react-native";
import Modal from "react-native-modal";
import { Header,Button } from 'react-native-elements';
import EditSport from './EditSport'
import EditTime from './EditTime'
import EditDistance from './EditDistance'
// import AddSport from './AddSport'
// import AddTime from './AddTime'
// import Adddistance from './Adddistance'
import WorkoutsCollections from '../api/WorkoutsCollection'


export default class EditWorkouts extends React.Component {
   

      render() {
        
        return (
          <View>
          
            <Modal
            animationInTiming = {50}
            animationOutTiming = {50}
            animationType={"slide"}
            backdropColor = '#1C1C21' 
            backdropOpacity = {1}
            transparent={false} 
            isVisible={this.props.isModalVisible}>
            
              <View style={{ flex: 1}}>
                
                <Header
                    leftComponent= {{ icon: 'cancel', size: 30,color: '#fff', onPress: () => this.props._toggleModal()}}
                    centerComponent={{ text: 'Edit Workout', style: { color: '#fff',fontSize: 25} }}
                    backgroundColor = '#1C1C21'
                  
                />
             
                <View style = {{flex: 0.8,justifyContent: 'space-around'}}>
                  <View style = {{flexDirection: 'row',justifyContent: 'space-between'}}>
                    <Text style = {{color:'white',fontSize:20, fontWeight: 'bold'}}>SPORT</Text>
                    <EditSport 
                    collectData = {this.props.collectData}
                    editworkout = {this.props.editworkout}
                    updateWorkouts= {this.props.updateWorkouts}
                    />
                    
                    }
                  </View>
                  <View style = {{flexDirection: 'row',justifyContent: 'space-between'}}>
                    <Text style = {{color: 'white',fontSize:20,fontWeight: 'bold'}}>DISTANCE</Text>
                    <EditDistance collectData = {this.props.collectData}
                     editworkout = {this.props.editworkout}
                     updateWorkouts = {this.props.updateWorkouts}
                     />
                    }
                  </View>
                  <View style = {{flexDirection: 'row',justifyContent: 'space-between'}}>
                    <Text style = {{color:'white',fontSize:20,fontWeight: 'bold'}}>TIME</Text>
                    <EditTime collectData = {this.props.collectData}
                     editworkout = {this.props.editworkout}
                     updateWorkouts= {this.props.updateWorkouts}
                     />
                    }
                  </View>
                  
                </View>
                <View  style = {{flex: 0.2,justifyContent: 'center',alignItems: 'center'}}>
                  <Button
                   onPress={()=>{
                    
                    this.props._toggleModal()
                   



                  }} 
                   
                   buttonStyle = {{width: 300}}
                   borderRadius = {5}
                   color = '#2DBFA4'
                   backgroundColor = 'white'
                   title='Close Window' 
                   textStyle = {{fontSize: 20,fontWeight: 'bold'}}/>
                </View>
              </View>
            </Modal>
          </View>
        );
      }

}