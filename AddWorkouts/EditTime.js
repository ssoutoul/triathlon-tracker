import React from 'react';
import { Text, TouchableOpacity,TouchableHighlight, View, Picker,StyleSheet} from "react-native";
import Modal from "react-native-modal";
import { Header, Button } from 'react-native-elements';
import TimePicker from 'react-native-simple-time-picker';

export default class EditTime extends React.Component {
    state = {
        selectedHours: '00',
        selectedMinutes: '00',
        time: '00:00'
       
      }

      state = {
        isModalVisible: false
       
      }

      isBiggerThan = num => {
        num < 10 && num >0 ? num = "0"+num : null
        return num
      }

      _toggleModal = () =>
        this.setState({ isModalVisible: !this.state.isModalVisible });
     
      render() {
        
        var newtime = ''
        if(this.props.editworkout) {
            newtime = this.props.editworkout.time
        }
        var { selectedHours, selectedMinutes, time} = this.state;
        selectedHours = selectedHours || '00'
        selectedMinutes = selectedMinutes || '00'
        time = selectedHours + 'h' + " " + this.isBiggerThan(selectedMinutes) + 'm';
        return (
           <View>
                <Button  buttonStyle = {{width: 150}} outline = {true} onPress={this._toggleModal} title={newtime} color = '#2DBFA4' backgroundColor = 'transparent'/>
                <Modal  isVisible={this.state.isModalVisible}
                
                >
                
                    <View style={styles.container}>
                        <Text>{time}</Text>
                        <TimePicker
                          
                        selectedHours={selectedHours}
                        selectedMinutes={selectedMinutes}
               
                        onChange={(hours, minutes) => this.setState({ selectedHours: hours, selectedMinutes: minutes})}
                        />
                    </View>
                    <TouchableOpacity>
                    <View style = {{flexDirection: 'row'}}>
                        <Button  onPress={this._toggleModal} backgroundColor = 'white' color = 'black' buttonStyle = {{width: 135,left:19,borderColor: 'gray',borderWidth: 1}} title='Cancel'/>
                        <Button   
                        onPress={()=>{
                          this.props.collectData('time',(time || this.props.editworkout.time))
                          this._toggleModal()

                          setTimeout(()=>{
                            this.props.updateWorkouts(this.props.editworkout._id)
                          },100)
                        }} 
                        backgroundColor = 'white' 
                        color = '#2DBFA4' 
                        buttonStyle = {{width: 135,right:11,borderColor: 'gray',borderWidth: 1}} 
                        title='Save'/>
                    </View>
                    </TouchableOpacity>
                
           
            </Modal>
          </View>
        );
      }
    }    
   
    const styles = StyleSheet.create({
        container: {
          flex: 0.4,
          backgroundColor: '#fff',
          alignItems: 'center',
          justifyContent: 'center',
          width: '80%',
          left: 34
        },
      });
      