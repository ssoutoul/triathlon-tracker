import React from 'react';
import { Text, TouchableOpacity,TouchableHighlight, View, Picker,StyleSheet} from "react-native";
import Modal from "react-native-modal";
import { Header, Button } from 'react-native-elements';
export default class EditSport extends React.Component {
    state = {
        isModalVisible: false
      };

   
    
      _toggleModal = () =>{
        this.setState({ isModalVisible: !this.state.isModalVisible });
    }
         
      render() {
       
          var sport = ''
        if(this.props.editworkout) {
            sport = this.props.editworkout.sport
        }
        
        return (
          <View>
 
            <Button  
            buttonStyle = {{width: 150}} 
            outline = {true} onPress={this._toggleModal} 
            title={sport} color = '#2DBFA4' backgroundColor = 'transparent'/>
            <Modal  isVisible={this.state.isModalVisible}
            
             >
              <View>
    
                <View>
                 <Picker
                    style =  {{ height: 200, width: '80%', alignSelf:'center',backgroundColor: 'white',borderColor: 'gray',borderWidth: 1}}
                    itemStyle = {{color: 'black'}}
                    selectedValue={this.state.language}
                    onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}>
                    <Picker.Item label="Swim" value="Swim" />
                    <Picker.Item label="Bike" value="Bike" />
                    <Picker.Item label="Run" value="Run" />
                      
                </Picker>
               
                </View>
                <TouchableOpacity>
                  <View style = {{flexDirection: 'row'}}>
                    <Button  onPress={this._toggleModal} backgroundColor = 'white' color = 'black' buttonStyle = {{width: 135,left:19,borderColor: 'gray',borderWidth: 1}} title='Cancel'/>
                    <Button   
                    onPress={()=>{
                      
                      this.props.collectData('sport',(this.state.language ||this.props.editworkout.sport))
                      this._toggleModal()

                      setTimeout(()=>{
                        this.props.updateWorkouts(this.props.editworkout._id)
                      },100)
                     
                      
                    }} 
                    backgroundColor = 'white' 
                    color = '#2DBFA4'
                    buttonStyle = {{width: 135,right:11,borderColor: 'gray',borderWidth: 1}} 
                    title='Save'/>
                  </View>
                </TouchableOpacity>
              </View>
            </Modal>
          </View>
        );
      }
}

