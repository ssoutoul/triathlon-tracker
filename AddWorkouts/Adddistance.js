import React from 'react';
import { StyleSheet,TextInput} from 'react-native';



export default class Adddistance extends React.Component {

  
  constructor(props) {
    super(props);
    this.state = { text: 'Placeholder' };
  }
    
    render() {
      
      return (
 
        <TextInput 
          style={{height:40, width:170, backgroundColor:'#1C1C21',color:'white'}}
          placeholder="ex.20km"
          placeholderTextColor='#D8D8D8'
          borderBottomWidth={1}
          borderBottomColor='white'
          keyboardType='numeric'
          returnKeyType = 'done'
          onChangeText={(text) => this.props.collectData('distance',text)}
         

        />


                  
      );
    }
  }


