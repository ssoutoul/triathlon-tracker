import React from 'react';
import { View,StyleSheet,TextInput} from 'react-native';
import {  Button } from 'react-native-elements';

export default class EditDistance extends React.Component {

  
  constructor(props) {
    super(props);
    this.state = { text: 'Placeholder' };
  }
    
    render() {
        var distance = ''
        if(this.props.editworkout) {
            distance = this.props.editworkout.distance
        }
      return (
        <View>
        <TextInput 
          style={{height:40, width:170, backgroundColor:'#1C1C21',color:'white'}}
          placeholder={distance}
          placeholderTextColor='#D8D8D8'
          borderBottomWidth={1}
          borderBottomColor='white'
          keyboardType='numeric'
          returnKeyType = 'done'
          onChangeText={(text) => this.props.collectData('distance',(text || this.props.editworkout.distance))}
         

        />

        <Button 

        onPress = {() => this.props.updateWorkouts(this.props.editworkout._id)}
        backgroundColor = 'white' 
        color = '#2DBFA4'
        buttonStyle = {{width: 65,top:11,borderColor: 'gray',borderWidth: 1}} 
        title='Save'/>
        </View> 
      );
    }
  }


