import React from 'react';
import { Icon } from 'react-native-elements';
import { Text, View ,StyleSheet,Switch,Alert} from "react-native";


export default class SwimAlerts extends React.Component {

    constructor (props) {
        super(props)
        this.state = {
          switchone:false,
          switchtwo:false,
          switchthree:false,

        }
      }

      switchOne = (value) => {
          debugger
        this.setState({ switchone: value
        })
      }

      switchTwo = (value) => {
          debugger
        this.setState({ switchtwo: value
         })
      }

      switchThree = (value) => {
          debugger
        this.setState({ switchthree: value
         })
      }


    render() {
        
        
        return (
          
              
              <View style = {styles.textalerts}>
                  <View style = {{flexDirection: 'row',justifyContent: 'space-around'}}>
                    <View>
                        <Text style = {{fontSize: 35,color: 'white',fontWeight: 'bold'}}>30%</Text>
                        <Text style = {{fontSize: 20,color: 'white'}}>Alert</Text>
                    </View>
                    <Switch
                        onValueChange={this.switchOne}
                        value={this.state.switchone}
                    />
    
                  </View>
                  <View style = {{flexDirection: 'row',justifyContent: 'space-around'}}>
                    <View>
                        <Text style = {{fontSize: 40,color: 'white',fontWeight: 'bold'}}>50%</Text>
                        <Text style = {{fontSize: 20,color: 'white'}}>Alert</Text>
                    </View>
                    <Switch
                        onValueChange={this.switchTwo}
                        value={this.state.switchtwo}
                        />
            
                  </View>
                  <View style = {{flexDirection: 'row',justifyContent: 'space-around'}}>
                    <View>
                        <Text style = {{fontSize: 40,color: 'white',fontWeight: 'bold'}}>100%</Text>
                        <Text style = {{fontSize: 20,color: 'white'}}>Alert</Text>
                    </View>
                    <Switch
                        onValueChange={this.switchThree}
                        value={this.state.switchthree}
                        />
                   
                  </View>
                </View>
              
          
          
    
          
        );
      }

}

const styles = StyleSheet.create({
    textalerts: {
        flex: 0.9,
        justifyContent: 'space-around',
        top: 30
        
    }
  })