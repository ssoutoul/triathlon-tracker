import React from 'react';
import { Icon } from 'react-native-elements';
import { Text, View ,StyleSheet,Switch,Alert} from "react-native";
import Modal from "react-native-modal";
import { Button } from 'react-native-elements';
import { Header } from 'react-native-elements';
import SwimAlerts from './SwimAlerts'


export default class Alerts extends React.Component {

    // constructor (props) {
    //     super(props)
    //     this.state = {
    //       switchone:false,
    //       switchtwo:false,
    //       switchthree:false,

    //     }
    //   }

    //   switchOne = (value) => {
    //     this.setState({ switchone: value,
    //      switchtwo:false,switchthree:false,
    //      })
    //   }

    //   switchTwo = (value) => {
    //     this.setState({ switchtwo: value,
    //      switchone:false,switchthree:false,
    //      })
    //   }

    //   switchThree = (value) => {
    //     this.setState({ switchthree: value,
    //      switchone:false,switchtwo:false,
    //      })
    //   }



    render() {
        
        
        return (
          <View style = {styles.alerts}>

                <Header
                    centerComponent={{ text: 'Alerts', style: { color: '#fff',fontSize: 25} }}
                    backgroundColor = '#1C1C21'
                />

               <View style = {styles.icons}>
              
                    <Icon 
                        reverse
                        name='swim' 
                        type= 'material-community'
                        color ='#2DBFA4'
                        size = {30}
                        
                    />
                    
                    <Icon 
                        reverse
                        name='directions-bike' 
                        color = '#E500A6'
                        size = {30}
                    
                    />
                    <Icon 
                        reverse
                        name='directions-run' 
                        color='#820384'
                        size = {30}
                    />
             </View>

             <SwimAlerts />
              
              {/* <View style = {styles.textalerts}>
                  <View style = {{flexDirection: 'row',justifyContent: 'space-around'}}>
                    <View>
                        <Text style = {{fontSize: 35,color: 'white',fontWeight: 'bold'}}>30%</Text>
                        <Text style = {{fontSize: 20,color: 'white'}}>Alert</Text>
                    </View>
                    <Switch
                        onValueChange={this.switchOne}
                        value={this.state.switchone}
                    />
    
                  </View>
                  <View style = {{flexDirection: 'row',justifyContent: 'space-around'}}>
                    <View>
                        <Text style = {{fontSize: 40,color: 'white',fontWeight: 'bold'}}>50%</Text>
                        <Text style = {{fontSize: 20,color: 'white'}}>Alert</Text>
                    </View>
                    <Switch
                        onValueChange={this.switchTwo}
                        value={this.state.switchtwo}
                        />
            
                  </View>
                  <View style = {{flexDirection: 'row',justifyContent: 'space-around'}}>
                    <View>
                        <Text style = {{fontSize: 40,color: 'white',fontWeight: 'bold'}}>100%</Text>
                        <Text style = {{fontSize: 20,color: 'white'}}>Alert</Text>
                    </View>
                    <Switch
                        onValueChange={this.switchThree}
                        value={this.state.switchthree}
                        />
                   
                  </View>
                </View> 
               */}
          </View>
          
    
          
        );
      }

}

const styles = StyleSheet.create({
    alerts: {
        flex:1
    },
    icons: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-around',
        top: 30
    }
    // textalerts: {
    //     flex: 0.9,
    //     justifyContent: 'space-around',
    //     top: 30
        
    // }
  })