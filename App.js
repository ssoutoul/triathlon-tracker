import React from 'react';
import Results from './Results/Results';
import Workouts from './Workouts/Workouts';
import AddWorkouts from './AddWorkouts/AddWorkouts';
import EditWorkouts from './AddWorkouts/EditWorkouts'
import Alerts from './Alerts/Alerts';
import { StyleSheet, Text, View} from 'react-native';
import Footer from './Footer';
import Goal from './Goal/Goal';
import Home from './Home/Home';
import Setup from './Setup_Goals/Setup'
import WorkoutsCollections from './api/WorkoutsCollection'
import GoalsAPI from './api/GoalsAPI'

const icons   = {
  'Swim': 'swim',
  'Bike': 'directions-bike',
  'Run':'directions-run'
}

const colors   = {
  'Swim': '#2DBFA4',
  'Bike': '#E500A6',
  'Run':'#820384'
}

export default class App extends React.Component {
  state = { 
    page :'Results', 
    isModalVisible: false,
    isModalPresent: false,
    workouts: [], 
    workout: ''
  } 

  componentDidMount () {
      
      this.findWorkouts()
      this.findAll()
      // WorkoutsCollections.remove({},{multi: true}, (e,d) => {
        
      // })
    }

    findWorkouts  = () =>{
        
        WorkoutsCollections.find({}, (e,d) => {
          
          this.setState({workouts:d})
         
          
        })
    }

    findAll= ()  => {
      debugger
      GoalsAPI.find({},(err, data)=>{
        debugger
        this.setState({sports:data})
      })
      
    }

    deleteWorkouts = (id) => {
      
      let { workouts } = this.state 
      WorkoutsCollections.remove({_id:id}, {multi: true},(e,d) => {
        this.findWorkouts()
      })
      
    }

    findID= (id) => {
      
      
      WorkoutsCollections.findOne({_id:id},(e,data) => {
        
        this.setState({editworkout:data})
        
        
      })
      
    }



  collectData = (action, data) => {
    
    this.setState({[action]:data}, () => {
      console.log(this.state)
      
    })
    
  }

  
  insertWorkouts = () => {
    
      var { distance, time, sport } = this.state
      var icon = icons[sport]
      var color = colors[sport]
          WorkoutsCollections.insert({
            distance:distance, 
            time, 
            sport,
            icon,
            color,
            swimtype: 'material-community'
          }, (e,d) => {
            this.findWorkouts()
          })
  
          // this.setState({workout: ''})
    }

    changePage = (page)=>{
      
      this.setState({page})
    }
  
    _toggleModal = () => {
      
      this.setState({ isModalVisible: !this.state.isModalVisible });
      
    }

    _toggleWindow = () => {
      
      this.setState({ isModalPresent: !this.state.isModalPresent });
      
    }
    
    updateWorkouts = (id) =>{
    
    // console.log('-----',this.state)
    var { sport } = this.state
    var { time } = this.state
    var { distance } = this.state
    
    var state = this.state
     
    // console.log(sport)
    WorkoutsCollections.update({_id:id},{$set:{
           sport: sport,
           time: time,
           distance: distance
    }},(err, done)=>{
      
      this.findID(id)
      this.findWorkouts()
      // WorkoutsCollections.find({},(err, res)=>{
        
      // })
      // 
    })
    }
      
  render(){
    let { page } = this.state
    let   shown

    if(page == 'Results') {
      shown = <Results 
      sport = {this.state.sport}
      findWorkouts = {this.findWorkouts}
      findAll =  {this.findAll}
      />
    }else if(page == 'Goal'){
      shown = <Goal
      findAll =  {this.findAll}
      findWorkouts = {this.findWorkouts}
      sports = {this.state.sports}/>
    }else if(page == 'Workouts'){
      shown = <Workouts 
      deleteWorkouts = {this.deleteWorkouts}
      workouts = {this.state.workouts}
      _toggleModal = {this._toggleWindow}
      findID = {this.findID}
      />
    } else if(page == 'Alerts'){
      shown = <Alerts />
    }

    return (
      <View style ={styles.container}>
      
      {shown}
      <AddWorkouts 
      isModalVisible = {this.state.isModalVisible} 
      _toggleModal = {this._toggleModal}
      collectData = {this.collectData}
      insertWorkouts = {this.insertWorkouts}
      />

      <EditWorkouts 
      banana = {this.state.editworkout}
      isModalVisible = {this.state.isModalPresent} 
      _toggleModal = {this._toggleWindow}
      collectData = {this.collectData}
      editworkout = {this.state.editworkout}
      updateWorkouts = {this.updateWorkouts}
      />

      <Footer changePage ={this.changePage} 
      _toggleModal = {this._toggleModal}
      />
      

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1C1C21'
  }, 
  
main:{
    flex:.9,
    top:50
  }
})


