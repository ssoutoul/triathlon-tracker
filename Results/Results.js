import React from 'react';
import Navbar from './Navbar'
import Graph from './Graph'
import Info from './Info'
import { StyleSheet, Text, View, Alert} from 'react-native';
import WorkoutsCollections from '../api/WorkoutsCollection'
import GoalsAPI from '../api/GoalsAPI'

export default class Results extends React.Component {
  state= {
    page:'Swimming',
    color: '#2DBFA4',
    icon: 'swim',
    type: 'material-community'
    
  }

  componentDidMount = () => {
    
    this.findBySport('Swim')
    this.findByGoal('Swim')
  }

  changePage = (page,color,icon,type)=>{
    this.setState({
      page,
      color,
      icon,
      type
      // textColor:'#2DBFA4'
    },()=>{
      console.log(this.state)
    })
  }

  
  findBySport = (sport) => {
    var distance = 0
    WorkoutsCollections.find({sport:sport},(err, data)=>{
        
        
        data.map(ele => distance += Number(ele.distance))
        this.setState({totaldistance:distance})
        
     
    })
    this.props.findWorkouts()
  }

  deleteWorkout = (sport) => {
    debugger
    WorkoutsCollections.remove({sport:sport},{multi: true}, (e,d) => {
        this.deleteGoal(sport)
        this.props.findWorkouts()
    })
  }

  deleteGoal = (sport) => {
    debugger
    GoalsAPI.remove({sport:sport},{multi: true}, (e,d) => {
        debugger
        this.findByGoal()
        this.findBySport()
        this.props.findAll()
    })
  }

    
  findByGoal = (sport) => {
    var percentage = 0
    
   
    GoalsAPI.find({sport:sport},(err, data)=>{
        
        data.map(ele => percentage += (Number(this.state.totaldistance) / Number(ele.distance))*100)
        
        this.setState({totalpercentage:Math.round(percentage)}, () => {
          
          if (this.state.totalpercentage > 99) {
            
            Alert.alert(
              'You have completed your goal.',
              'Congratulations!',
              
              [
                {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'Reset Goal', onPress: () => this.deleteWorkout(sport)},
              ],
              { cancelable: false }
            )
          }
          if(this.state.totalpercentage > 100) {
            this.setState({totalpercentage:100})
            
          }
        })

    })
  
  }
 
    render() {
   
      return (
        <View style = {styles.results}>
          <Navbar changePage ={this.changePage}  
          findBySport = {this.findBySport}
          findByGoal = {this.findByGoal}
          />
          <View style={styles.graph}>
          <Graph 
          color = {this.state.color}
          totalpercentage = {this.state.totalpercentage}
          // textColor = {this.state.textColor}
          />
          </View>
          <Info 
          page = {this.state.page}
          icon = {this.state.icon}
          color = {this.state.color}
          type = {this.state.type}
          totaldistance = {this.state.totaldistance}
          
          />
        </View>
  
        
      );
    }
  }
  
  const styles = StyleSheet.create({
    results: {
        flex:1,
        
    },
    graph: {
        flex: 0.47,
        justifyContent: 'center',
        alignItems: 'center'
      
      
      }
    
  });
  
  
  