import React from 'react';
import { Icon } from 'react-native-elements'
import { StyleSheet, Text, View} from 'react-native';

export default class Info extends React.Component {
    render() {
      
      return (
       
        <View style = {styles.info}>
            <Icon 
            name= {this.props.icon} 
            type = {this.props.type}
            color={this.props.color}
            size= {110}
            
            />
            <View style = {{width: 130}}>
                <Text style = {{color: 'white',fontSize: 25,fontWeight: "bold"}}>{this.props.page}</Text>
                <Text style = {{color: 'white',fontSize: 20,marginLeft: 15}}>{this.props.totaldistance} km</Text>
            </View>
        </View>
  
        
      );
    }
  }
  
  const styles = StyleSheet.create({
    info: {
      flex: 0.2,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-around'

    }
   
  });
  
  
  