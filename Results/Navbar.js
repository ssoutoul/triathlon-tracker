import React from 'react';
import { Icon } from 'react-native-elements'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { StyleSheet, Text, View} from 'react-native';
import { Header } from 'react-native-elements';

export default class Navbar extends React.Component {
    render() {
      return (
            <View style = {styles.navbar}>
                <Header
                    centerComponent={{ text: 'Results', style: { color: '#fff',fontSize: 25} }}
                    backgroundColor = '#1C1C21'
                  
                />
                <View style = {styles.icons}>
              
                <Icon 
                    // onPress = 
                    // {() =>  this.props.changePage('Swimming','#2dbfa4','swim','material-community')}
                    onPress={()=>{
                        this.props.changePage('Swimming','#2dbfa4','swim','material-community')
                        this.props.findBySport("Swim")
                        this.props.findByGoal("Swim")
                      }} 
                    reverse
                    name='swim' 
                    type= 'material-community'
                    color ='#2DBFA4'
                    size = {30}
                     
                />
              
                <Icon 
                    onPress = 
                    {() =>  {
                        this.props.changePage('Biking', '#e500a6','directions-bike','MaterialIcons')
                        this.props.findBySport("Bike")
                        this.props.findByGoal("Bike")
                    }}
                    reverse
                    name='directions-bike' 
                    color = '#E500A6'
                    size = {30}
                
                />
                <Icon 
                    onPress = 
                    {() =>  {
                        this.props.changePage('Running','#820384','directions-run','MaterialIcons')
                        this.props.findBySport("Run")
                        this.props.findByGoal("Run")
                    }}
                    reverse
                    name='directions-run' 
                    color='#820384'
                    size = {30}
                />
            </View>
        </View>
  
        
      );
    }
  }

  const styles = StyleSheet.create({
    navbar: {
      flex: 0.3
    },
    icons: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-around',
        top: 30
    }
  });
  
  
  