import React from 'react';
import { Icon } from 'react-native-elements';
//import { ProgressCircle }  from 'react-native-svg-charts';
import PercentageCircle from 'react-native-percentage-circle';
import { StyleSheet, Text, View} from 'react-native';

export default class Graph extends React.PureComponent {
    // calculate = () => {
    //   Number(workoutdistance)/Number(goaldistance) * 100 
    // }
    render() {

      
      return (
        <View>
        <PercentageCircle 
        radius={130} 
        percent={this.props.totalpercentage} 
        color={this.props.color} 
        textStyle = {{
          fontSize:60, 
          color: 'white',
          fontWeight: 'bold'
        }} 
        borderWidth = { 20 } 
        innerColor = '#1C1C21'>
        </PercentageCircle>  
      </View>
           
        
  
        
      )
    }
  }
  
 
  